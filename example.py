from reordering import *

print(f"{can_be_swapped(Read(1, 'x'), Read(1, 'y')) = }")
print(f"{can_be_swapped(Read(1, 'x'), Read(2, 'y')) = }")
print(f"{can_be_swapped(Read(1, 'x'), Read(2, 'x')) = }")
print(f"{can_be_swapped(Write(1, 'x'), Read(2, 'y')) = }")
print(f"{can_be_swapped(Read(1, 'x'), Write(2, 'y')) = }")
print(f"{can_be_swapped(Write(1, 'x'), Read(2, 'x')) = }")
print(f"{can_be_swapped(Read(1, 'x'), Write(2, 'x')) = }")
print(f"{can_be_swapped(Write(1, 'x'), Write(2, 'y')) = }")
print(f"{can_be_swapped(Write(1, 'x'), Write(2, 'x')) = }")
print(f"{can_be_swapped(Write(1, 'x'), Write(1, 'y')) = }")

# schedule = (Read(1, 'x'), Write(1, 'y'), Read(2, 'x'), Read(2, 'y'), Read(3, 'y'), Write(3, 'y'), Write(3, 'z'))
# schedule = (Read(2, 'x'), Read(1, 'x'), Write(2, 'x'), Read(1, 'y'), Read(2, 'y'), Write(2, 'y'), Write(1, 'z'))
schedule = (Read(1, 'x'), Write(1, 'y'), Read(2, 'x'), Read(2, 'y'), Read(3, 'y'), Write(3, 'y'), Write(1, 'z'))

print(f'{schedule = }')

descendants = {}
print(f'{explore(schedule, descendants) = }')

plot(descendants)
