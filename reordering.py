from dataclasses import dataclass

import pydot


@dataclass(frozen=True)
class Operation:
    transaction: int
    operand: str

    def __repr__(self):
        return f'{type(self).__name__}{self.transaction}({self.operand})'


class Read(Operation):
    pass


class Write(Operation):
    pass


def can_be_swapped(op1, op2):
    if op1.transaction == op2.transaction:
        return False

    match (op1, op2):
        case Read(), Read():
            return True
        case Read(_, o1), Write(_, o2) if o1 != o2:
            return True
        case Write(_, o1), Read(_, o2) if o1 != o2:
            return True
        case Write(_, o1), Write(_, o2) if o1 != o2:
            return True
        case _:
            return False


def children(schedule):
    for i, (op1, op2) in enumerate(zip(schedule, schedule[1:])):
        if can_be_swapped(op1, op2):
            yield (*schedule[:i], op2, op1, *schedule[i+2:])


def explore(schedule, descendants):
    if schedule in descendants:
        return

    descendants[schedule] = []

    for child in children(schedule):
        descendants[schedule].append(child)

        if child not in descendants:
            explore(child, descendants)

    return descendants


def is_serial(schedule):
    seen = set()
    active = None
    for operation in schedule:
        transaction = operation.transaction

        if transaction == active:
            continue

        if transaction in seen:
            return False

        active = transaction
        seen.add(active)
    return True


def plot(descendants):
    graph = pydot.Dot('schedule', graph_type='graph')
    graph.set_strict(True)

    for schedule, children in descendants.items():
        schedule_name = str(schedule)[1:-1]
        graph.add_node(pydot.Node(name=schedule_name, shape='box', peripheries=1 + is_serial(schedule)))
        for child in children:
            child_name = str(child)[1:-1]
            graph.add_edge(pydot.Edge(schedule_name, child_name))

    graph.write_pdf('descendants.pdf')
